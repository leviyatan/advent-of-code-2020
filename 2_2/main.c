#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER 50

int main(){
    FILE *p_input = fopen("input.txt", "r");

    int ch = 0;
    int total_lines = 0;
    while(!feof(p_input)){
        ch = fgetc(p_input);
        if(ch == '\n') total_lines++;
    }

    rewind(p_input);

    char line_array[total_lines][BUFFER];

    int i = 0;
    while(!feof(p_input)){
        fgets(line_array[i], BUFFER, p_input);
        i++;
    }

    int min[total_lines];
    int max[total_lines];
    char character[total_lines];
    char password[total_lines][BUFFER];

    for(int i = 0; i < total_lines; i++){
        sscanf(line_array[i], "%d-%d %c: %s", &min[i], &max[i], &character[i], &password[i]);
    }

    int passcount = 0;
    for(int i = 0; i < total_lines; i++){
        if((character[i] == password[i][min[i]-1]) ^ (character[i] == password[i][max[i]-1])){
            passcount++;
        }
    }

    printf("Solution: %d\n", passcount);

    return 0;
}