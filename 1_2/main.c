#include <stdio.h>
#include <stdlib.h>

#define BUFFER 50

int main(){
    FILE *p_input = fopen("input.txt", "r");

    int ch = 0;
    int total_lines = 0;
    while(!feof(p_input)){
        ch = fgetc(p_input);
        if(ch == '\n') total_lines++;
    }

    rewind(p_input);

    char line[BUFFER];
    int a_input[total_lines];

    int i = 0;
    while(!feof(p_input)){
        fgets(line, BUFFER, p_input);
        a_input[i] = atoi(line);
        i++;
    }

    fclose(p_input);

    int solution = 0;

    for(int i = 0; i < total_lines; i++){
        for(int j = 0; j < total_lines; j++){
            for(int k = 0; k < total_lines; k++){
                if(i != j != k){
                    if(a_input[i] + a_input[j] + a_input[k] == 2020){
                        solution = a_input[i] * a_input[j] * a_input[k];
                    }
                }
            }
        }
    }

    printf("Solution: %d\n", solution);

    return 0;
}